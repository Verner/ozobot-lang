import inspect
import math
import random

from assembler import decode_byte, unwrap_program, frame2bytes


class Stack:
    def __init__(self):
        self.stacks = [[]]
        self.return_adrs = []
        pass

    def call(self, return_ip):
        self.stacks.append([val for val in self.stacks[-1]])
        self.return_adrs.append(return_ip)

    def pick(self, depth):
        self.stacks[-1].append(self.stacks[-2][-(depth+1)])

    def put(self, value, depth):
        self.stacks[-2][-(depth+1)]=value

    def ret(self):
        del self.stacks[-1]
        adr = self.return_adrs[-1]
        del self.return_adrs[-1]
        return adr

    def pop(self, num=1):
        ret = self.stacks[-1][-num:]
        self.stacks[-1]=self.stacks[-1][:-num]
        return ret

    def peek(self, depth=0):
        return self.stacks[-1][-(depth+1)]

    def push(self, val):
        self.stacks[-1].append(val)

class Ozobot:
    SENSOR_MAP = {
        'COLOR': 14,
        'LINE': 10,
        'LINE_SPEED': 0x18,
        'INTERSECTION_COLOR':0x0f,
    }
    LAST_ID=-1
    class NotImplemented(Exception):
        pass

    class ProgramStopped(Exception):
        pass

    def __init__(self, world=None):
        Ozobot.LAST_ID += 1
        self.id = Ozobot.LAST_ID
        self.stack = Stack()
        self.vars = [0]*256
        self.code = []
        self.world = None
        if world is not None:
            world.place_bot(self)
        self.ip = 0 # Instruction pointer

    def _get_bytecode_at(self, adr, signed=False):
        bc = self.code[adr]
        if not signed or bc < 128:
            return bc
        else:
            return (bc-256)

    def _inc_ip(self, amount=1):
        self.ip += amount

    def _sample_sensors(self):
        if self.world is None:
            return
        for sensor, var in self.SENSOR_MAP.items():
            self.vars[var] = self.world.read(self, sensor)


    def load_program(self, code):
        unwrapped = unwrap_program(code)
        self.code=unwrapped

    def load_frame(self, frame):
        self.load_program(frame2bytes(frame))

    def step(self):
        instr = decode_byte(self.code[self.ip])
        if type(instr) == int:
            self.stack.push(instr)
        else:
            self._sample_sensors()
            method_name = instr.replace(' ','_').lower()
            try:
                method = getattr(self, method_name)
            except:
                raise Ozobot.NotImplemented("Instruction "+instr+" not implemented.")
            sig = inspect.signature(method)
            num_args = len(sig.parameters)
            if num_args > 0:
                args = self.stack.pop(num_args)
            else:
                args=[]
            ret = method(*args)
            if ret is not None:
                self.stack.push(ret)
        self._inc_ip()

    def run(self, max_steps=10000):
        for st in range(max_steps):
            try:
                self.step()
            except Ozobot.ProgramStopped:
                return

    # INTERACTING WITH WORLD
    def move(self, distance, speed):
        if self.world:
            self.world.move(self, distance, speed)

    def set_led(self, red, green, blue):
        if self.world:
            self.world.led(self, red, green, blue)

    def set_speed(self, left, right):
        if self.world:
            self.world.speed(self, left, right)

    def rotate(self, degree, speed):
        if self.world:
            self.world.rotate(self, degree, speed)

    def wait(self, msec):
        if self.world:
            self.world.wait(self, msec)

    # Boolean
    def gt(self, x, y):
        return x > y

    def geq(self, x, y):
        return x >= y

    def eq(self, x, y):
        return x == y

    def b_not(self, x):
        return not x

    def b_or(self, x, y):
        return x or y

    def b_and(self, x,y):
        return x and y

    # MATH
    def mod(self, x, y):
        return x % y

    def div(self, x, y):
        return x / y

    def mul(self, x, y):
        return x * y

    def sub(self, x, y):
        return x - y

    def add(self, x, y):
        return x + y

    def abs(self, value):
        return abs(value)

    def neg(self, num):
        return (-(num+1))

    def rand(self, min, max):
        return random.randint(min, max)

    # STACK & VARS
    def drop(self):
        self.stack.pop()

    def pop(self, num):
        self.stack.pop(num)

    def put(self, value, depth):
        self.stack.put(value, depth)

    def pick(self, depth):
        return self.stack.pick(depth)

    def set_var(self, val, var):
        self.vars[var] = val

    def get_var(self, var):
        return self.vars[var]


    # BRANCHING & CONTROL
    def call(self):
        target = self._get_bytecode_at(self.ip+1)*256+self._get_bytecode_at(self.ip+2)
        self.stack.call(self.ip+3)
        self.ip = target-1

    def ret(self):
        self.ip = self.stack.ret()-1


    def cond_jump(self, condition):
        if condition:
            jump_length = self._get_bytecode_at(self.ip+1, signed=True)
            self._inc_ip(jump_length-1)
        else:
            self._inc_ip()

    def jump(self):
        jump_length = self._get_bytecode_at(self.ip+1, signed=True)
        self._inc_ip(jump_length-1)

    def exit(self, status):
        if self.world:
            self.world.stop(self, status)
        else:
            raise Ozobot.ProgramStopped(status)








