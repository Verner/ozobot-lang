#!/usr/bin/env python

import curses
import sys
import time

import assembler
import simulator
import world

def gotoxy(x,y):
    print ("%c[%d;%df" % (0x1B, y, x), end='')




class ConsoleWorld(world.AbstractWorld):
    def __init__(self, scr):
        super().__init__()
        self.scr = scr
        self.bot_screens = {}
        self.active_bot_win = {
            'bot_id':None,
            'win':0
        }
        self.win_order = ['code','stack','vars']
        self.win_len = {
            'code':21,
            'stack':5,
            'vars':7
        }

    def place_bot(self, bot, position=None):
        super().place_bot(bot, position)
        # TODO: Handle multiple bots
        bot_win = self.scr.derwin(19,40,2,0)
        self.bot_screens[bot.id] = {
            'win':bot_win,
            'code':bot_win.derwin(15,21,3,1),
            'stack':bot_win.derwin(15,5,3,23),
            'vars':bot_win.derwin(15,7,3,30),
            'code_pos':0,
            'max_code':len(bot.code)-1,
            'stack_pos':0,
            'max_stack':0,
            'vars_pos':25,
            'max_vars':255,
        }
        self.active_bot_win['bot_id']=bot.id


    def show_bot(self, bot, advance_code=True):
        scrs = self.bot_screens[bot.id]
        scrs['win'].clear()
        hpos=1
        for wo, w in enumerate(self.win_order):
            if wo == self.active_bot_win['win']:
                scrs['win'].addstr(1,hpos,w.upper().center(self.win_len[w]),curses.A_BOLD)
            else:
                scrs['win'].addstr(1,hpos,w.upper().center(self.win_len[w]))
            hpos+=self.win_len[w]+1
        scrs['win'].hline(2,1,curses.ACS_HLINE,38)
        scrs['win'].vline(3,21,curses.ACS_VLINE,16)
        scrs['win'].vline(3,29,curses.ACS_VLINE,16)
        scrs['win'].box()

        code_w = scrs['code']
        code_h = code_w.getmaxyx()[0]

        if advance_code and (abs(bot.ip-scrs['code_pos']) >= code_h or bot.ip < scrs['code_pos']):
            scrs['code_pos']=max(int(bot.ip-code_h/2),0)

        for i in range(0,min(code_h,len(bot.code)-scrs['code_pos'])):
            ctx = {
                'addr':i+scrs['code_pos'],
                'instr':assembler.decode_byte(bot.code[i+scrs['code_pos']]),
                'indicator':'  '
            }
            if i+scrs['code_pos'] == bot.ip:
                ctx['indicator']="=>"
            code_w.addstr(i,0,"{indicator} {addr:3} {instr}".format(**ctx))

        stack_w = scrs['stack']
        stack_h = stack_w.getmaxyx()[0]
        for i in range(1,min(stack_h,len(bot.stack.stacks[-1]))+1-scrs['stack_pos']):
            stack_w.addstr(i-1,0," {:3d}".format(bot.stack.stacks[-1][-(scrs['stack_pos']+i)]))

        vars_w = scrs['vars']
        vars_h = vars_w.getmaxyx()[0]
        for i in range(min(vars_h,len(bot.vars)-scrs['vars_pos'])):
            vars_w.addstr(i,0,"{0:3d} {1:2x}".format(scrs['vars_pos']+i, bot.vars[scrs['vars_pos']+i]))


    def handle_key(self, k):
        if k == curses.KEY_UP:
            pos_key = self.win_order[self.active_bot_win['win']]+'_pos'
            scrs = self.bot_screens[self.active_bot_win['bot_id']]
            scrs[pos_key] = max(0,scrs[pos_key]-1)
            self.print_status(advance_code=False)
            return True

        if k == curses.KEY_DOWN:
            base = self.win_order[self.active_bot_win['win']]
            pos_key = base+'_pos'
            m_key = 'max_'+base
            scrs = self.bot_screens[self.active_bot_win['bot_id']]
            scrs[pos_key] = min(scrs[m_key],scrs[pos_key]+1)
            self.print_status(advance_code=False)
            return True

        if k == curses.KEY_RIGHT:
            self.active_bot_win['win'] = (self.active_bot_win['win']+1)%3
            self.print_status(advance_code=False)
            return True

        if k == curses.KEY_LEFT:
            self.active_bot_win['win'] = (self.active_bot_win['win']-1)%3
            self.print_status(advance_code=False)
            return True

        if k == ord(' '):
            self.tick()
            return True

        return False

    def print_status(self, advance_code=True):
        self.scr.clear()
        self.scr.addstr("Console Ozobot Simulator. Press 'q' to quit.")
        self.scr.addstr(1,0,"Time: {time:03.2f}, Total Bots: {tot!s} (active: {active!s}, ready: {ready!s})".format(
            time=self.clock,
            ready=len(self.ready),
            active=len(self.active),
            tot=len(self.bots)
        ))

        for num, bot in enumerate(self.bots.values()):
            self.show_bot(bot['bot'], advance_code=advance_code)
        self.scr.refresh()

    def post_tick(self):
        self.print_status()





class App:
    def __init__(self, scr):
        self.scr = scr
        self.scr.keypad(True)
        self.w = ConsoleWorld(self.scr)
        curses.curs_set(False)

    def add_bot_from_frame(self, program_frame):
        bot = simulator.Ozobot()
        bot.load_frame(program_frame)
        self.w.place_bot(bot)

    def add_bot_from_bytes(self, bytecode):
        bot = simulator.Ozobot()
        bot.load_program(bytecode)
        self.w.place_bot(bot)

    def run(self, tick_time=None):
        self.scr.nodelay(True)
        self.w.print_status()
        last_tick = time.time()
        while True:
            c = self.scr.getch()
            if c == ord('q'):
                break
            self.w.handle_key(c)
            if tick_time is not None:
                if time.time()-last_tick > tick_time:
                   last_tick = time.time()
                   self.w.tick()

    def exit(self):
        curses.curs_set(True)
        self.scr.keypad(False)

def main(screen, bots=[]):
    app = App(screen)
    for bot in bots:
        if bot.endswith('rgb'):
            code_frame = open(bot,'r').read()
            app.add_bot_from_frame(code_frame)
        elif bot.endswith('b'):
            code = list(map(int,open(bot,'rb').read()))
            app.add_bot_from_bytes(code)
    app.run()
    app.exit()

def run(screen):
    if __name__ == "__main__":
        main(screen, sys.argv[1:])

curses.wrapper(run)
