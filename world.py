class AbstractWorld:
    def __init__(self):
        self.bots = {}
        self.clock = 0
        self.clock_tick_len = 0.1 # msec
        self.start=(0,0)
        self.active = {}
        self.ready = {}

    def place_bot(self, bot, position=None):
        if position is None:
            position = self.start
        self.bots[bot.id] = {
            'bot':bot,
            'pos':position,
            'direction':0,
            'left_speed':0,
            'right_speed':0,
            'led':(0,0,0),
            'command':None,
            'arg':None,
            'c_speed':None
        }
        bot.world = self
        self.ready[bot.id]=bot

    def tick(self):
        self.pre_tick()
        done = []
        active  = {}
        for bot, command, args in list(self.active.values()):
            args = self.do(bot, command, *args)
            if args is None:
                self.done.append(bot)
            else:
                self.active[bot.id] = (bot, command, args)

        self.active = active

        for bot in done:
            self.ready[bot.id] = bot

        for bot in self.ready.values():
            bot.step()

        self.clock += self.clock_tick_len
        self.post_tick()

    def update(self, bot, property, value):
        self.bots[bot.id]['property'] = value

    def post_tick(self):
        pass

    def pre_tick(self):
        pass

    def get(self, bot, property, default=None):
        return self.bots[bot.id].get(property, default)

    def do(self, bot, command, *args):
        method = getattr(self, 'do_'+command, None)
        if method is None:
            return None
        return method(bot, *args)

    def activate(self, bot_id, command, *args):
        self.active[bot_id] = (self.bots[bot_id], command, args)
        del self.ready[bot_id]

    def stop(self, bot, status):
        self.update(bot, 'exit_status', status)
        del self.ready[bot.id]

    def move(self, bot, distance, speed):
        self.update(bot, 'left_speed', 0)
        self.update(bot, 'right_speed', 0)
        self.activate(bot, 'move', distance, speed)

    def rotate(self, bot, degree, speed):
        self.activate(bot, 'rotate', degree, speed)

    def wait(self, bot, ms):
        self.activate(bot, 'wait', ms)

    def led(self, bot, r, g, b):
        self.update(bot, 'led', (r,g,b))

    def speed(self, bot, left, right):
        self.update(bot, 'left_speed', left)
        self.update(bot, 'right_speed', right)

    def read(self, bot, sensor):
        return 0


    def do_move(self, bot, distance, speed):
        direction = self.get(bot, 'direction')
        pos = self.get(bot, 'pos')
        d = self.clock_tick_len*speed
        d = min(d,distance)
        pos[0] += math.sin(direction)*d
        pos[1] += math.cos(direction)*d
        self.update(bot,'pos',pos)
        distance = distance-d
        if distance > 0:
            return (distance, speed)
        else:
            return None

    def do_rotate(self, bot, degree, speed):
        if degree < 0:
            delta = max(degree, -self.clock_tick_len*speed)
        else:
            delta = min(degree, self.clock_tick_len*speed)
        self.update(bot, 'direction', self.get('bot','direction')+delta)
        degree -= delta
        if abs(degree) < 0.0000001:
            return None
        else:
            return (degree, speed)

