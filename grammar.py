from textx.metamodel import metamodel_from_file
from textx.export import model_export

def indent(ln):
    ret = 0
    for c in ln:
        if not c == ' ':
            return ret
        ret +=1
    return ret

def preprocess(code):
    lines = code.split('\n')
    indent_level=0
    ret = []
    for ln in lines:
        if len(ln.strip()) == 0:
            ln = 'pass'
        il = indent(ln)
        if il > indent_level:
            ret.append('{'+ln[1:])
        elif il < indent_level:
            ret[-1] += '}'
            ret.append(ln)
        else:
            ret.append(ln)
        indent_level = il
    return ';\n'.join(ret)

bozo_lang = metamodel_from_file('grammar.tx')

c = open('example.bzy', encoding='utf8').read()
c = preprocess(c)
program = bozo_lang.model_from_str(c)
model_export(program,'example.dot')
