# Ozobotlang

   A compiler targetting the [Ozobot](htttp://ozobot.com) robot. The compiler currently compiles an 'assembly'-like
   language into [flash-codes](http://ozobot.com/play/color-code-language) which can be uploaded
   onto the Ozobot.

# Ozobot Processor

   The Ozobot processor is a simple stack machine. A program for this machine consists of a
   sequence of bytes wrapped in a simple header (containing a version number, the length of the program)
   and a footer (containing a simple checksum). To upload a program onto the Ozobot, this sequence
   is encoded into a sequence of colors (R,G,B,C,M,Y,K) and again it is wrapped in a header and footer (signalling
   the start of program upload and stop of program uplod) which can than be flashed at the robot's sensors
   (e.g. hold the bot agains a computer screen flashing these colors).  The encoding is a simple base-7 encoding
   with the digits being, in order from 0 to 6, `K R G Y B M C`. Since the bot only registers changes to colors,
   whenever there is a pair of two adjacent identical colors the second color is replaced by a `W`.

   The sequence of instructions for the program consists of 8-bit numbers. Each number represents either an
   instruction (numbers > 127) or a value which is pushed onto the stack. The processor also has 255 registers
   which can be accessed via special instructions. The program is free to use registers starting from 25. Registers
   0-25 are read-only and accessing them gives sensor readings. The following table is compiled from the
   [documentation](https://github.com/AshleyF/ozobot) written by [AshleyF](https://github.com/AshleyF) who reverse-engineered
   the bot. Currently it probably contains some misinformation (e.g. the order of the arguments might be switched, etc.).

## Instruction set

### Senzors & Wheels

| **Bytecode** | **Instruction** | **Notes**                                                                                        |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------ |
| 0xb8         | `SET LED  `     | Consumes three parameters (`r`, `g`, `b`) sets led light to the rgb color.                       |
| 0x9f         | `SET SPEED`     | Consumes two parameters (left wheel speed, right wheel speed;|mm/s]) and sets the wheel speed.   |
| 0x9e         | `MOVE     `     | Consumes two parameters (distance|mm | speed|mm/s]) and moves the robot forward.                 |
| 0x98         | `ROTATE   `     | Consumes two parameters (degree|deg | speed|deg/s]) and turns the robot by the given degree.     |
| 0x9b         | `WAIT     `     | Consumes a single parameter (time|msec]) and waits for the given amount of time.                 |

### Logic

| **Bytecode** | **Instruction** | **Notes**                                                                                        |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------ |
| 0xa2         | `B AND`         | Consumes two parameters and pushes their boolean conjunction (`AND`) onto the stack.             |
| 0xa3         | `B OR `         | Consumes two parameters and pushes their boolean disjunction (`OR`) onto the stack.              |
| 0x8a         | `B NOT`         | Consumes a single parameters and pushes its boolean negation (`NOT`) onto the stack.             |
| 0xa4         | `EQ   `         | Consumes two parameters and pushes `True` onto the stack if they are equal and `False` otherwise |
| 0x9c         | `GEQ  `         | Consumes two parameters and pushes `True` onto the stack if the first is >= the second,          |
|              | `     `         | `False` otherwise.                                                                               |
| 0x9d         | `GT   `         | Consumes two parameters and pushes `True` onto the stack if the first is > the second, `False`   |
|              |                 | otherwise.                                                                                       |

### Math

| **Bytecode** | **Instruction** | **Notes**                                                                                        |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------ |
| 0x85         | `ADD `          | Consumes two parameters and pushes their sum onto the stack.                                     |
| 0x86         | `SUB `          | Consumes two parameters and pushes their difference (first - second) onto the stack.             |
| 0x87         | `MUL `          | Consumes two parameters and pushes their product onto the stack.                                 |
| 0x88         | `DIV `          | Consumes two parameters and pushes the result of integer dividing the first by the second onto   |
|              |                 | the stack.                                                                                       |
| 0x89         | `MOD `          | Consumes two parameters and pushes the remainder of integer division of the first by the second  |
|              |                 | onto the stack.                                                                                  |
| 0x8b         | `NEG `          | Consumes one parameter `A` and pushes `-(A+1)` onto the stack.                                   |
| 0xa8         | `ABS `          | Consumes one parameter and pushes its absolute value onto the stack.                             |
| 0x8c         | `RAND`          | Consumes two parameters `N`, `M` and pushes a random integer from the interval `[N,M]` onto the  |
|              |                 | stack.                                                                                           |

### Stack & Variable access

| **Bytecode** | **Instruction** | **Notes**                                                                                        |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------ |
| 0x93         | `SET VAR`       | Consumes two parameters `V`, `R` and puts the value `V` into register `R`.                       |
| 0x92         | `GET VAR`       | Consumes one parameter `R` and pushes the value of the register `R` onto the stack.              |
| 0x96         | `DROP   `       | Removes the top element from the stack.                                                          |
| 0xa7         | `POP    `       | Consumes a single parameter `N` and removes `N`-elements from the top of the stack.              |
| 0xa5         | `PICK   `       | Consumes a single parameter `N` and pushes the n-th element of the caller's stack onto the       |
|              |                 | calee's stack. (see functions)                                                                   |
| 0xa6         | `PUT    `       | Consumes two parameters `N`, `V` and puts value `V` into the n-th position from the top of the   |
|              |                 | caller's stack. (see functions)                                                                  |

### Control Flow

| **Bytecode** | **Instruction** | **Notes**                                                                                        |
| ------------ | --------------- | ------------------------------------------------------------------------------------------------ |
| 0xba         | `JUMP`          | Does not consume anything. Execution continues from position which is determined by `IP+J`,      |
|              |                 | where `IP` is the current position (address of the `JUMP` instruction) and `J` is the number     |
|              |                 | from `[-128,128)` which immediately succeeds the `JUMP` instruction.                             |
| 0x80         | `COND JUMP`     | Consumes a single boolean value. If this value is true, behaves exactly as `JUMP`. Otherwise it  |
|              |                 | skips the next value and continues execution after it.                                           |
| 0x90         | `CALL`          | Does not consume anything. Execution transfers to the address given by the next two bytes. Upon  |
|              |                 | encountering the `RET` instruction, execution continues right after the two address bytes.       |
| 0x91         | `RET`           | Returns from a function call.                                                                    |
| 0xae         | `EXIT`          | Consumes a single parameter and finishes executing the program.                                  |

# Ozobot assembly

  Although the goal is to eventually have the compilar recognize a restricted subset of [Python](https://www.python.org),
  currently it recognizes only a simple "assembly"-type language which it can translate into a ozobot program suitable for
  uploading to the ozobot. A line of code has either of the the following three formats:

## Macro definition

```asm
   DEF name value
```

  which is defines a simple macro. Any occurance of `$name` in the program is replaced, verbatim, by value. Note
  that neither value nor name can contain spaces.

## Compound statement

```asm
  [label:] INSTR ARG1[, ARG2 [,ARG3 ...]]
```

  where the optional `label` gives a symbolic name to the line (for use with `JUMP`, `CALL`), `INSTR` is an instruction
  name. Single word names from the above table will work. Names having a space in them must be replaced according to the
  following table:


| **INSTUCTION** | **SHORTCUT** |
| -------------- | ------------ |
| `SET LED`      | `SLED  `     |
| `SET SPEED`    | `SSPEED`     |
| `B AND`        | `&&    `     |
| `B OR`         | `||    `     |
| `B NOT`        | `!     `     |
| `COND JUMP`    | `CJUMP `     |
| `GET VAR`      | `LOAD`       |
| `SET VAR`      | `STORE`      |


  Additionnaly, the compiler recognizes the `INC` and `DEC` instruction, which increases/decreases the variable given
  by the first argument by 1 (or by the optional second argument). Arguments are the arguments to the instruction,
  the result, if any, is pushed onto the stack. The arguments may either be literal numbers from the range
  `[-128,128)`, characters with ASCII code < 128 or registers `V#` (where `#` is the number identifying the register).
  The `JUMP` instructions are treated in a special way. Their argument must be either a literal number (the amount of lines
  by which to jump, must be in the range `[-128,128)`) or a symbolic line label (which must not be more than 127 instructions
  away---note that there will be about 3 instructions per line). The call instruction accepts a either a symbolic line label
  or the two-byte integer address of where the function body starts.

## Literal

```asm
  VALUE
```

  Value, which may be either a character with ASCII code < 128, an integer in the range `[-128,128)` or a variable name V#.
  The corresponding value is pushed onto the stack.

# Example program

```asm
      DEF i V15
loop: SLED $i, 0, 127 // Set leds to r={content of register 15}, g=0, b=127
      INC $i          // Increments the value of register 15 ($i=V15)
      GEQ $i, 100     // Puses the Boolean result of the comparison V15 >= 100
      CJMP loop       // If the previous result was False, jumps to the line
                      // labeled loop
      EXIT 0          // Finishes the program and puts the bot into basic mode.
```

# ozcc.py

The ozlang compiler. For help, run the program with the `-h` option:

```bash
$ ozcc.py -h
```

To compile a file into RGB codes suitable for flashing at the bot, do

```bash
$ ozcc.py src.asm -o out.rgb
```

By using the `--format` option, you can also make ozcc.py output the intermediate simple assembly or
the bytecode version of the program (including header & footer with checksum).

# Thanks

   [AshleyF](https://github.com/AshleyF) for [reverse engineering](https://github.com/AshleyF/ozobot) the ozobot.
