Program:
  statements*=Statement[';'];

Statement:
    (
      EmptyStatement |
      DefStatement |
      ReturnStatement |
      ForStatement |
      WhileStatement |
      IfStatement |
      BlockStatement |
      FunctionCallExpression |
      AssignStatement |
      Expression
    );

EmptyStatement:
  'pass';

BlockStatement:
    BEGIN
        statements*=Statement[';']
    END;

AssignStatement:
    IdentTuple '=' Tuple;

Expression:
    FunctionCallExpression | ListLiteral | AritmExpression | BooleanExpression;

AritmExpression:
    Sum;

Sum:
    Product ((op='+'|op='-') r=Product)*;

Product:
    NumberValue (op='*' r=NumberValue)*;

NumberValue:
    ListElement | MyID | INT | ('(' AritmExpression ')');

BooleanExpression:
    And ( op='or' r=And)*;

And:
    Neg ( op='and' r=Neg)*;

Neg:
    op='not' l=SimpleBool | SimpleBool;

SimpleBool:
    l=AritmExpression op='==' r=AritmExpression |
    l=AritmExpression op='<' r=AritmExpression |
    l=AritmExpression op='<=' r=AritmExpression |
    l=AritmExpression op='>' r=AritmExpression |
    l=AritmExpression op='>=' r=AritmExpression |
    ListElement | MyID | BOOL |  '(' BooleanExpression ')';


ListElement:
    (lst=MyID | lst=ListLiteral) '[' index=AritmExpression ']';

ListLiteral:
    '[' elements*=Expression[','] ']';

ForStatement:
    'for' loop_var=MyID 'in' loop_list=Expression ':' EOL
        body=BlockStatement;

WhileStatement:
    'while' condition=Expression ':' EOL
        body=BlockStatement;

IfStatement:
    'if' condition=BooleanExpression ':' EOL
        true_body=BlockStatement
    (EOL
        'elif' elif_cond=BooleanExpression ':' EOL
            elif_body=BlockStatement
    )*
    (EOL
        'else' ':' EOL
            else_body=BlockStatement
    )*;

IdentTuple:
    identifiers*=MyID[','];

Tuple:
    elements*=Expression[','];

FunctionCallExpression:
  var_name=MyID '.' method_name=MyID '(' arguments=Tuple ')' |
  function_name=MyID '(' arguments=Tuple ')';


DefStatement:
  'def' function_name=MyID '(' arguments=IdentTuple ')' ':' EOL
        body=BlockStatement;

ReturnStatement:
  'return' ret_val=Expression;

Comment:
  /#.*$/
;

Keyword: 'def' | 'for' | 'or' | 'and' | 'not' | 'False' | 'True' | 'if' | 'elif' | 'else' | 'while' | 'return';

MyID: !Keyword ID;

EOL: ';';
BEGIN: '{';
END: '}';
