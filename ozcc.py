#!/usr/bin/env python

import argparse
import logging
import sys

from assembler import assemble, wrap_program, bytes2frame

logger = logging.getLogger(__name__)

def parse_args():
    parser = argparse.ArgumentParser(description='An Ozobot compiler')
    parser.add_argument('--verbose', '-v', action='count',help='be verbose',default=0)
    parser.add_argument('--pure', '-p', help='Disable extended ASM', default=False)
    parser.add_argument('--output_fmt', '-f', choices=['bytes','rgb', 'simpleasm'], help='output format', default='rgb')
    parser.add_argument('--output', '-o', help='write output to file',default=None)
    parser.add_argument('source', help='Source to compile', default=None)
    return parser.parse_args()

def main():
    args = parse_args()
    logger.setLevel(logging.ERROR-args.verbose*10)
    if args.source is not None:
        src = open(args.source,'r').readlines()
    else:
        try:
            src = []
            while True:
                src.append(input())
        except EOFError:
            pass

    if not args.pure:
        src = assemble(src, extended=True)

    if args.output_fmt == 'simpleasm':
        output = '\n'.join(map(str,src))
        logger.warn("Final length: "+str(len(src)))
    else:
        output = assemble(src, extended=False)
        logger.warn("Unwrapped length:"+str(len(output)))
        output = wrap_program(output)
        logger.warn("Wrapped length:"+str(len(output)))
        if args.output_fmt == 'rgb':
            output = bytes2frame(output)
        else:
            output = bytes(output)
        logger.warn("Final length:"+str(len(output)))
    if args.output is None:
        if type(output)==bytes:
            sys.stdout.buffer.write(output)
        else:
            print(output)
    else:
        if type(output)==bytes:
            o=open(args.output,'wb')
        else:
            o=open(args.output,'w')
        o.write(output)

if __name__ == "__main__":
    main()

