"""
Copyright (c) 2017, Jonathan Verner <jonathan@temno.eu>

Thanks to https://github.com/AshleyF for reverse engineering
the Ozobot.

"""

import re

BYTECODE_MAP = {
    0xb8:'SET LED',
    0x8b:'NEG',
    0x9e:'MOVE',
    0x98:'ROTATE',
    0x9f:'SET SPEED',
    0x9b:'WAIT',
    0xae:'EXIT',
    0xa2:'B AND',
    0xa3:'B OR',
    0x8a:'B NOT',
    0xa4:'EQ',
    0x9c:'GEQ',
    0x9d:'GT',
    0x85:'ADD',
    0x86:'SUB',
    0x87:'MUL',
    0x88:'DIV',
    0x89:'MOD',
    0x8c:'RAND',
    0x93:'SET VAR',
    0x92:'GET VAR',
    0x90:'CALL',
    0x91:'RET',
    0x96:'DROP',
    0xa5:'PICK',
    0xa7:'POP',
    0xa6:'PUT',
    0xa8:'ABS',
    0xba:'JUMP',
    0x80:'COND JUMP'
}

DOC = [
    # Senzors & Wheels
    [ 0xb8,'SET LED', "Consumes three parameters (r, g, b) sets led light to the rgb color."],
    [ 0x9f,'SET SPEED', "Consumes two parameters (left wheel speed, right wheel speed; [mm/s]) and sets the wheel speed." ],
    [ 0x9e,'MOVE', "Consumes two parameters (distance [mm], speed [mm/s]) and moves the robot forward."],
    [ 0x98,'ROTATE',"Consumes two parameters (degree [deg], speed [deg/s]) and turns the robot by the given degree." ],
    [ 0x9b,'WAIT', "Consumes a single parameter (time [msec]) and waits for the given amount of time." ],

    # Logic
    [ 0xa2,'B AND', "Consumes two parameters and pushes their boolean conjunction (`AND`) onto the stack." ],
    [ 0xa3,'B OR', "Consumes two parameters and pushes their boolean disjunction (`OR`) onto the stack." ],
    [ 0x8a,'B NOT', "Consumes a single parameters and pushes its boolean negation (`NOT`) onto the stack." ],
    [ 0xa4,'EQ', "Consumes two parameters and pushes `True` onto the stack if they are equal and `False` otherwise." ],
    [ 0x9c,'GEQ', "Consumes two parameters and pushes `True` onto the stack if the first is >= the second, `False` otherwise." ],
    [ 0x9d,'GT', "Consumes two parameters and pushes `True` onto the stack if the first is > the second, `False` otherwise." ],

    # Math
    [ 0x85,'ADD', "Consumes two parameters and pushes their sum onto the stack." ],
    [ 0x86,'SUB', "Consumes two parameters and pushes their difference (first - second) onto the stack."  ],
    [ 0x87,'MUL', "Consumes two parameters and pushes their product onto the stack."  ],
    [ 0x88,'DIV', "Consumes two parameters and pushes the result of integer dividing the first by the second onto the stack."  ],
    [ 0x89,'MOD', "Consumes two parameters and pushes the remainder of integer division of the first by the second onto the stack."  ],
    [ 0x8b,'NEG', "Consumes one parameter A and pushes -(A+1) onto the stack." ],
    [ 0xa8,'ABS', "Consumes one parameter and pushes its absolute value onto the stack."  ],
    [ 0x8c,'RAND', "Consumes two parameters N, M and pushes a random integer from the interval [N,M] onto the stack."  ],

    # Stack & Variable access
    [ 0x93,'SET VAR', "Consumes two parameters V, R and puts the value V into register R."],
    [ 0x92,'GET VAR', "Consumes one parameter R and pushes the value of the register R onto the stack." ],
    [ 0x96,'DROP', "Removes the top element from the stack." ],
    [ 0xa7,'POP' , "Consumes a single parameter N and removes N-elements from the top of the stack." ],
    [ 0xa5,'PICK', "Consumes a single parameter N and pushes the n-th element of the caller's stack onto the calee's stack. (see functions)" ],
    [ 0xa6,'PUT', "Consumes two parameters N V and puts value V into the n-th position from the top of the caller's stack. (see functions)" ],

    # Control Flow
    [ 0xba,'JUMP', """Does not consume anything. Execution continues from position which is determined by IP+J, where IP is
     the current position (address of the JUMP instruction) and J is the number from [-128,128) which immediately succeeds
     the JUMP instruction."""],
    [ 0x80,'COND JUMP', "Consumes a single boolean value. If this value is true, behaves exactly as JUMP. Otherwise it skips the next value and continues execution after it." ],
    [ 0x90,'CALL', "Does not consume anything. Execution transfers to the address given by the next two bytes. Upon encountering the RET instruction, execution continues right after the two address bytes." ],
    [ 0x91,'RET', "Returns from a function call." ],
    [ 0xae,'EXIT', "Consumes a single parameter and finishes executing the program." ],

]

EXTENDED_MAP = {
    'SLED':'SET LED',
    'SSPEED': 'SET SPEED',
    '&&': 'B AND',
    '||': 'B OR',
    '!': 'B NOT',
    'JMP': 'JUMP',
    'CJMP': 'COND JUMP',
    '>': 'GT',
    '>=': 'GEQ',
    '==': 'EQ',
    'STORE': 'SET VAR',
    'LOAD': 'GET VAR'
}

INSTRUCTION_MAP = {v: k for k, v in BYTECODE_MAP.items()}

FRAME_START=['CRY', 'CYM', 'CRW']
FRAME_END=['CMW']
VERSION = [0x1, 0x3, 219]


NUM_MAP = {
    0:'K',
    1:'R',
    2:'G',
    3:'Y',
    4:'B',
    5:'M',
    6:'C'
}

COL_MAP = {v: k for k, v in NUM_MAP.items()}

def tobase(n, base):
    ret = []
    while n > 0:
        ret.append(n%base)
        n = int(n/base)
    ret.reverse()
    return ret

def bytes2frame(bytes):
    coded = []
    coded.extend(FRAME_START)
    for b in bytes:
        coded.extend(map(lambda x:NUM_MAP[x],tobase(b,7)))
    # replace repeats with W:
    for i in range(len(coded)):
        if i > 0 and coded[i-1] == coded[i]:
            coded[i] = 'W'
    coded.extend(FRAME_END)
    return ''.join(coded)

def frame2bytes(cols):
    cols=cols.strip()
    if cols[:len(''.join(FRAME_START))] != ''.join(FRAME_START):
        raise ValueError("Sequence not a valid frame. Needs to start with: "+ \
                         ''.join(FRAME_START) + "(got: " +\
                         ''.join(cols[:len(''.join(FRAME_START))])+" instead)")
    elif cols[-len(''.join(FRAME_END)):] != ''.join(FRAME_END):
        raise ValueError("Sequence not a valid frame. Needs to end with: "+ \
                         ''.join(FRAME_END) + "(got: " +\
                         ''.join(cols[-len(''.join(FRAME_END)):])+" instead)")
    # Strip the frame envelope
    cols = list(cols[len(''.join(FRAME_START)):-len(''.join(FRAME_END))])
    for pos, col in enumerate(cols):
        if col == 'W':
            cols[pos]=cols[pos-1]

    pos = 0
    ret = []
    while pos < len(cols):
        triple=cols[pos:pos+3]
        digits=''.join(map(lambda x:str(COL_MAP[x]), triple))
        ret.append(int(digits, base=7))
        pos+=3
    return ret

def wrap_program(code):
    wrapped = VERSION+[int(len(code)/256),len(code)%256]+code
    checksum = 0
    for b in wrapped:
        checksum -= b
    return wrapped+[checksum%256]

def unwrap_program(code):
    if code[:len(VERSION)] != VERSION:
        raise ValueError("Unknown version: " + '.'.join(map(str,code[:len(VERSION)])))
    header_len= code[len(VERSION)]*256+code[len(VERSION)+1]
    if len(code) != header_len+len(VERSION)+3:
        raise ValueError("Invalid program length: " + str(len(code))+" (expected: "+str(header_len+len(VERSION)+3)+")")
    checksum=0
    for b in code[:-1]:
        checksum -= b
    if code[-1] != checksum%256:
        raise ValueError("Invalid checksum: "+str(checksum%256)+"(expected: "+str(code[-1])+")")
    return code[len(VERSION)+2:-1]

class AssemblerError(BaseException):
    def __init__(self, line, instr, message, error_type=''):
        super().__init__(message)
        self.line = line
        self.instr = instr
        self.message = message
        self.error_type = error_type

    def __str__(self):
        return "Error {etype!s} at {ln!s}: {message!s} (instr: {code!s})".format(
            etype=self.error_type,
            ln=self.line,
            message=self.message,
            code=self.instr
        )

LINE_RE = re.compile("""
    (?P<label>\s*[^:]*:)?
    \s*
    (?P<instr>[^ ]*)\s
    \s*
    (?P<args>[^ ][^/]*)
    (?P<comment>//.*)?
    $
""", re.VERBOSE)

def get_var(pos, val):
    try:
        var = int(val[1:])
    except:
        raise AssemblerError(pos, val, "Invalid variable name: must be a number", 'invalid value')
    if not 0 <= var < 256:
        raise AssemblerError(pos, val, "Invalid variable name: must be in range [0,256)", 'out of range')
    return var

def expand_stack_value(pos, val):
    expanded = []
    # Push variable onto stack
    if val[0] == 'V':
        var = get_var(pos, val)
        expanded.append(var)
        expanded.append('GET VAR')

    # Push Literal value onto stack
    else:
        try:
            byte = int(val)
        except:
            if len(val) == 1:
                byte = ord(val)
            else:
                raise AssemblerError(pos, val, "Literals must be integers or single characters", 'invalid value')
        if not -128 <= byte < 128:
            raise AssemblerError(pos, val, "Literals must be values in the range [-128,128), got:"+str(byte), 'out of range')
        if byte >= 0:
            expanded.append(byte)
        # Handle negative literals
        else:
            expanded.append(abs(byte+1))
            expanded.append('NEG')
    return expanded


class Reference:
    def __init__(self, mapping, anchor=None, jump=None, target=None):
        self.anchor = anchor
        self.jump = jump
        self.target = target
        self.mapping = mapping
        if target is None:
            self.len=1
            self.relative=True
        else:
            self.len=2
            self.relative=False

    def resolve(self):
        if self.relative:
            if type(self.jump) == int:
                expanded = self.mapping.expansion
                pos = self.mapping.resolve(self.anchor)
                dist = abs(self.jump)
                step = int(self.jump/dist)
                ln=0
                while dist > 0:
                    pos += step
                    ln += step
                    if expanded[pos][0] is not None:
                        dist -=1
                if ln < 0:
                    ln=256+ln
                if ln > 255:
                    raise AssemblerError(None, str(self),'Attempted long jump use call instead:'+str(ln),'out of range')
                return [ln]
            else:
                pos = self.mapping.resolve(self.anchor)
                target = self.mapping.resolve(self.jump)
                if abs(target-pos) > 255:
                    raise AssemblerError(None, str(self),'Attempted long jump use call instead:'+str(target-pos),'out of range')
                else:
                    ln = target-pos
                    if ln < 0:
                        ln = 256+ln
                    return [ln]
        else:
            tgt = self.mapping.resolve(self.target)
            return [int(tgt/256), tgt%256]

    def __repr__(self):
        if self.relative:
            return '@'+str(self.anchor)+'+'+str(self.jump)
        else:
            return '@'+str(self.target)

class Mapping:
    def __init__(self):
        self.symbolic_targets = {}
        self.line_mapping = {}

    def define(self, source, target):
        self.symbolic_targets[source] = target

    def load_expansion(self, expanded):
        npos = 0
        for pos, _ in expanded:
            if pos is not None:
                self.line_mapping[pos] = npos
            npos+=1
        self.expansion = expanded

    def resolve(self, target):
        try:
            target = int(target)
            return self.line_mapping[target]
        except:
            old_line = self.symbolic_targets[target]
            return self.line_mapping[old_line]

def assemble_extended(code):
    expanded = []
    labels = Mapping()
    defs = {}
    replace_address=None

    # Process macro definitions
    for ln in code:
        ln = ln.strip()
        if ln.startswith('DEF'):
            _, name, val = ln.split(' ')
            defs[name] = val

    for pos, ln in enumerate(code):
        instructions = []
        ln=ln.strip()

        if ln.startswith('DEF') or ln.startswith('//') or ln == '':
            continue

        # Expand macro definitions
        for name, val in defs.items():
            ln=ln.replace('$'+name, val)

        m = LINE_RE.match(ln)
        if m:
            # Extended instruction
            label = m.group('label')
            instr = m.group('instr')
            args = [a.strip() for a in m.group('args').split(',')]
            if instr in EXTENDED_MAP:
                instr = EXTENDED_MAP[instr]
            if label is not None:
                labels.define(label[:-1], pos)
            if instr in ['JUMP', 'COND JUMP', 'CALL']:
                instructions.append(instr)
                if instr == 'CALL':
                    ref = Reference(labels, target=args[0])
                else:
                    try:
                        jump = int(args[0])
                        if not -128 <= jump < 128:
                            raise AssemblerError(pos, ln, "Argument to jump must be an integer in [-128,128), got:"+str(jump),'invalid value')
                    except ValueError:
                        jump = args[0]
                    ref = Reference(labels, anchor=pos, jump=jump)
                instructions.append(ref)
                instructions.extend(["SKIP"]*(ref.len-1))
            elif instr in ['INC', 'DEC']:
                if not 1 <= len(args) <= 2:
                    raise AssemblerError(pos, ln, "INC/DEC expects at least 1 and at most 2 arguments, got:"+str(args),'wrong arguments')
                if args[0][0] != 'V':
                    raise AssemblerError(pos, ln, "First argument to INC must be a variable",'invalid type')
                else:
                    var = get_var(pos, args[0])
                instructions.extend(expand_stack_value(pos,args[0]))
                if len(args) == 1:
                    args.append('1')
                instructions.extend(expand_stack_value(pos,args[1]))
                if instr == 'INC':
                    instructions.append('ADD')
                else:
                    instructions.append('SUB')
                instructions.extend([var, 'SET VAR'])
            elif instr in ['SET VAR']:
                if not 1 <= len(args) <= 2:
                    raise AssemblerError(pos, ln, "STORE expects at least 1 and at most 2 arguments, got:"+str(args),'wrong arguments')
                if args[0][0] != 'V':
                    raise AssemblerError(pos, ln, "First argument to STORE must be a variable, got: "+args[0]+"instead",'invalid type')
                var = get_var(pos, args[0])
                if len(args) > 1:
                    instructions.extend(expand_stack_value(pos, args[1]))
                instructions.append(var)
                instructions.append(instr)
            else:
                for v in args:
                    instructions.extend(expand_stack_value(pos, v))
                instructions.append(instr)
        else:
            # Plain instruction
            if ln in INSTRUCTION_MAP or ln in EXTENDED_MAP:
              if ln in EXTENDED_MAP:
                  ln = EXTENDED_MAP[ln]
              if ln in ['JUMP', 'COND JUMP', 'CALL']:
                  raise AssemblerError(pos, ln, "Bare JUMP/COND JUMP/CALL not supported", 'not supported')
              instructions.append(ln)
            else:
                seq = expand_stack_value(pos, ln)
                instructions.extend(seq)
        expanded.append((pos,instructions[0]))
        expanded.extend([(None,s) for s in instructions[1:]])

    ret = []
    npos = 0

    labels.load_expansion(expanded)
    for _, instr in expanded:
        if instr == 'SKIP':
            continue
        elif isinstance(instr, Reference):
            try:
                ret.extend(instr.resolve())
            except AssemblerError as e:
                e.inst = instr
                raise e
            except:
                raise AssemblerError(_, instr, "Unable to resolve reference "+str(instr), 'reference error')
        else:
            ret.append(instr)
    return ret



def assemble_simple(code):
    ret = []
    for pos, ln in enumerate(code):
        if ln in INSTRUCTION_MAP:
            ret.append(INSTRUCTION_MAP[ln])
        else:
            try:
                byte=int(ln)
            except:
                raise AssemblerError(pos, ln, "Values must be a number between 0 and 127", 'invalid value')
            ret.append(byte)
    return ret

def assemble(code, extended=True):
    if extended:
        return assemble_extended(code)
    else:
        return assemble_simple(code)

def decode_byte(b):
    if b > 127:
        return BYTECODE_MAP.get(b, 'UNKNOWN ('+str(b)+')')
    else:
        return b

def disassemble(code):
    ret = []
    for b in code:
        ret.append(decode_byte(b))
    return ret
